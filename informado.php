<?php
define("_VALID_", true);
require_once('libs/config.php');
include_once("classes.php");

function guardar(){
	$mysql = new MysqlDB();
	$grupos = $mysql->get("grupos");
	$iptrafData ="00e052a6bbd2:Lan server\nf46d0491b59b:Internet server\nDC9FDB005E11:Antena Oran\nDC9FDB005DE9:Antena Est Volcan Higueras\nDC9FDB005D7C:Antena Emi Volcan Higueras\nDC9FDB089B8D:Antena Est Morado\n000C421698A3:Antena Emi Morado\n00156D634F3C:Nodo Iruya\n00156D68A6AB:AP 2.4 Iruya\nb0487ae1875c:Balanceador";
	foreach ($grupos as $grupo) {
		$hosts = $mysql->query("SELECT host.mac, host.nombre, clientes.nombre as cliente  FROM host left join clientes on (host.idCliente = clientes.id) where host.estado = 'activo' AND clientes.grupo=".$grupo['id']);
		$data = '';
		foreach ($hosts as $host) {
			$data .= strtoupper(trim($host['mac']))."\n";
			$iptrafData .= "\n".strtolower(str_replace(":", "", $host['mac'])).":".str_replace(" ", "_", $host['cliente'])." ".str_replace(" ", "_", $host['nombre']);
		}
		$data .= '#fin';
		$file = PATH_GRUPOS.$grupo['file'];
		if(file_exists($file)){
			unlink($file);
		}

		$fp = fopen( $file , 'w' );
		fwrite ( $fp , $data );
		fclose ( $fp );
	}

	$file = "/var/lib/iptraf/ethernet.desc";
	if(file_exists($file)){
		unlink($file);
	}

	$fp = fopen( $file , 'w' );
	fwrite ( $fp , $iptrafData );
	fclose ( $fp );
	exec("sudo /etc/init.d/./iptable.sh", $output);
	exec("sudo service squid3 reload", $output);	
}
if (isset($_POST['cliente']) && $_POST['cliente'] != '') { //
	$id = $_POST['cliente'];
	$datos = array(
		'grupo' => 1,
	);
	$mysql = new MysqlDB();
	$mysql->where('id',$id);
	$result = $mysql->update('clientes',$datos);
	if ($result) {
		echo "Sus datos se han guardado correctamente. <br> Muchas gracias por su tiempo, siga disfrutando de su servicio.";
		guardar();
	}else 
		echo "error 1. ";
}
else 
	echo "error 2. ";

?>