<?php
define("_VALID_", true);
require_once('libs/config.php');
include_once("classes.php");

function getMac(){
	if( isset( $_SERVER['HTTP_X_FORWARDED_FOR'] )) $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	else if( isset( $_SERVER ['HTTP_VIA'] ))  $ip = $_SERVER['HTTP_VIA'];
	else if( isset( $_SERVER ['REMOTE_ADDR'] ))  $ip = $_SERVER['REMOTE_ADDR'];
	else $ip = null ;

	$mac = '';
	$respuesta = exec("sudo arp $ip");
	$rep = str_replace("-", ":", $respuesta);
	if (preg_match("/[0-9a-f][0-9a-f][:][0-9a-f][0-9a-f][:][0-9a-f][0-9a-f][:][0-9a-f][0-9a-f][:][0-9a-f][0-9a-f][:][0-9a-f][0-9a-f]/",$rep, $coincidencias))
		$mac = $coincidencias[0];
	return strtoupper($mac);
}

$mysql = new MysqlDB();
$query = "SELECT clientes.* FROM clientes LEFT JOIN  host ON (clientes.id=host.idCliente) WHERE host.mac='".getMac()."'";
$cliente = $mysql->query($query);
$cliente = @$cliente[0];

$id = (isset($cliente['id'])) ? base64_encode($cliente['id']):'';
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Datos Clientes - IruyaNet</title>
	<script src="libs/js/jquery.min.js"></script>
	<link rel="stylesheet" href="templates/css/mensajes.css">
<script>
$(function(){

	$(".botonenviar").on('click', function() {
		var validar = function (){
			var data = {
				direccion: $("#direccion").val(),
				celu: $("#celu").val(),
				email: $("#email").val()
			};
			if ((data.direccion.trim()).length < 4) {
				$(".error").text('Por favor Complete correctamente la Direccion.').show('fast');
				$("#direccion").focus();
				return false;
			};
			if ((data.celu.trim()).length < 6) {
				$(".error").text('Por favor Complete correctamente el Numero de celular.').show('fast');
				$("#celu").focus();
				return false;
			};
			if (!/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/.test($.trim(data.email))) {
				$(".error").text('Por favor Complete correctamente el Email.').show('fast');
				$("#email").focus();
				return false;
			};
			$(".error").text('').hide('fast');
			return true;
		}

		if(validar())
			$("#datos").submit();
	});
});
</script>
</head>
<body>
	<header>
		<!--<h1>Eterea Comunicaciones</h1>-->
		<img src="templates/img/logo.png" alt="IruyaNet">
	</header>
	<section class="content">
		<h2>Datos Clientes - IruyaNet</h2>

		<p>Estimado Cliente: <strong><?php echo @ $cliente["nombre"] ?></strong><br>
			Con motivos de actualizar nuestra bases de datos y asi comunicarnos mas directamente con usted, le solicitamos encarecidamente actualizar sus datos de contacto.</p>
		<p class="aclaracion">En cuanto complete el formulario se restaurara el servicio al instante. Esto no es un corte de servicio.
			<br>Solo debe completar con sus datos el siguiente Formulario y presionar guardar...</p>
		<form action="guardar-datos.php?id=<?php echo $id ?>" method="post" id="datos">
			<fieldset>
				<legend>Datos:</legend>
				<p class="error"></p>
				<p><label for="direccion">Direccion:  </label><br>
				<input type="text" min="5" value="<?php echo @ $cliente["direccion"] ?>" id="direccion" name="direccion"></p>

				<p><label for="celu">Celular:  </label><br>
				<input type="text" class="numero" value="<?php echo @ $cliente["celu"] ?>" id="celu" name="celu"><small> (ej: 3875245142)</small></p>

				<p><label for="email">Email:  </label><br>
				<input type="text" value="<?php echo @ $cliente["email"] ?>" id="email" name="email"></p>
				<p style="text-align:center;padding:0;margin:0;"><a href="javascript:;" class="botonenviar">Guardar</a></p>
				
			</fieldset>			
		</form>
	</section>
	<section class="datos">
		<div style="float:right">
			<img src="templates/img/qr.png" style="height:110px">
		</div>
		<h3>Datos de Contacto</h3>
			Email: cesardluis@gmail.com<br>
			Celular: 0387 -5967496
	</section>
	<footer>
		&copy; 2014 IruyaNet
	</footer>
	
</body>
</html>