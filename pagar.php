<?php
define("_VALID_", true);

require_once('libs/config.php');
include_once("classes.php");

function getMac(){
	if( isset( $_SERVER['HTTP_X_FORWARDED_FOR'] )) $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	else if( isset( $_SERVER ['HTTP_VIA'] ))  $ip = $_SERVER['HTTP_VIA'];
	else if( isset( $_SERVER ['REMOTE_ADDR'] ))  $ip = $_SERVER['REMOTE_ADDR'];
	else $ip = null ;

	$mac = '';
	$respuesta = exec("sudo arp $ip");
	$rep = str_replace("-", ":", $respuesta);
	if (preg_match("/[0-9a-f][0-9a-f][:][0-9a-f][0-9a-f][:][0-9a-f][0-9a-f][:][0-9a-f][0-9a-f][:][0-9a-f][0-9a-f][:][0-9a-f][0-9a-f]/",$rep, $coincidencias))
		$mac = $coincidencias[0];
	return strtoupper($mac);
}

$mysql = new MysqlDB();
$query = "SELECT clientes.nombre, clientes.id FROM clientes LEFT JOIN  host ON (clientes.id=host.idCliente) WHERE host.mac='".getMac()."'";
$cliente = $mysql->query($query);
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>IruyaNet</title>
	<link rel="stylesheet" href="templates/css/mensajes.css">
</head>
<body>
	<header>
		<!--<h1>Eterea Comunicaciones</h1>-->
		<img src="templates/img/logo.png" alt="IruyaNet">
	</header>
	<section class="content">
		<h2 style="color:red">Servicio Suspendido!</h2>
		<p>Sr(a). <strong><?php echo @$cliente[0]["nombre"] ?></strong></p>
		<p>Señor cliente le informamos que su conexión ha sido suspendida, ya que su cuenta registra facturas impagas, queremos recordarle que los abonos son generados por mes adelantado y vencen los  días 10 de cada mes en curso.</p>
		<p><strong>Lugar de Pago</strong>: San Martin esquina Retiro o Cyber IruyaNet a lado del complejo(Korea).</p>
		<strong>Cobrador: </strong>Cesar Luis Diaz (viernes de 19:00 hasta 21:00).-
		<p><strong>Aclaración:</strong> en caso de no regularizar su servicio, el mismo quedara suspendido hasta tanto pague su deuda (cobrando un adicional por mora).</p>
		<p>Si considera que este aviso es un error, puede informar sus pagos comunicándose con nosotros al 0387 - 5967496 ó personalmente. Sepa disculpar las molestias ocasionadas.</p>

		<form action="informado.php" method='post'>
			<input type="hidden" name="cliente" value="<?php echo @$cliente[0]["id"] ?>"><br>
			Hoy Viernes se estara cobrando a partir de horas 19:00, tener en cuenta que es el ultimo dia de pago.
			<input type="submit" value="Haga Click AQUI para seguir navegando." class="boton">
		</form>
		<p>Saludos!!</p>
	</section>
	<section class="datos">
		<div style="float:right">
			<img src="templates/img/qr.png" style="height:110px">
		</div>
		<h3>Datos de Contacto</h3>
			Email: cesardluis@gmail.com<br>
			Celular: 0387 -5967496
	</section>
	<footer>
		&copy; 2014 IruyaNet
	</footer>

</body>
</html>