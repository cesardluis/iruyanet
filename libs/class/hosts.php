<?php
class hosts{
	var $id;
	var $mysql;
	var $opcion;
	var $vars;
	public function __construct($vars= array()){
		$this->mysql = new MysqlDB();
		$this->vars = $vars;
		if(isset($vars['opcion']))
			$this->opcion = $vars['opcion'];
		else
			$this->opcion = 'listaClientes';

	}
	private function listaClientes(){
		//obtener todos los hosts

		$where = "";
		if ($_SESSION['session']['localidad'] != "all") {
			$where ="AND clientes.localidad = ".$_SESSION['session']['localidad'];
		}
		$hosts = $this->mysql->query("SELECT host.*, clientes.nombre as cliente, clientes.antena FROM host LEFT JOIN  clientes ON (host.idCliente=clientes.id)
		WHERE clientes.grupo != 5 $where;");


		//obtener los host conectados
		exec("sudo arp -n -i eth0", $output);

		$arp = array();
		$conectados = array();

		foreach ($output as $value) {
			$conectado = false;
			preg_match('/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/',$value, $ip);
			preg_match("/[0-9a-f][0-9a-f][:][0-9a-f][0-9a-f][:][0-9a-f][0-9a-f][:][0-9a-f][0-9a-f][:][0-9a-f][0-9a-f][:][0-9a-f][0-9a-f]/",$value, $mac);

			foreach ($hosts as $h => $host) {
				if (strtolower($host["mac"]) == $mac[0]) {
					$host["ip"] = $ip[0];
					$conectados[] = $host;
					$conectado = true;
					unset($hosts[$h]);
					break;
				}

			}
			if(! $conectado)
				$arp[] =array(
					"ip" => $ip[0],
					"mac" => $mac[0]
				);
		}

        header("content-type: application/x-javascript");
        echo json_encode(array_merge($conectados, $hosts));
	}
	private function getIp($mac){
		$ip = "";
		$respuesta = exec("sudo ip ne | grep ".strtolower($mac));
		$rep = explode(" ", $respuesta);

		foreach ($rep as $val) {
			if (preg_match('/^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/',$val))
				$ip = $val;
		}
		return $ip;
	}
	private function getMac(){
	    if( isset( $_SERVER['HTTP_X_FORWARDED_FOR'] )) $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    else if( isset( $_SERVER ['HTTP_VIA'] ))  $ip = $_SERVER['HTTP_VIA'];
	    else if( isset( $_SERVER ['REMOTE_ADDR'] ))  $ip = $_SERVER['REMOTE_ADDR'];
	    else $ip = null ;

	    $mac = '';
		$respuesta = exec("sudo arp $ip");
		$rep = str_replace("-", ":", $respuesta);
		if (preg_match("/[0-9a-f][0-9a-f][:][0-9a-f][0-9a-f][:][0-9a-f][0-9a-f][:][0-9a-f][0-9a-f][:][0-9a-f][0-9a-f][:][0-9a-f][0-9a-f]/",$rep, $coincidencias))
			$mac = $coincidencias[0];
		return strtoupper($mac);
	}
	public function mostrar(){
        $opcion = $this->opcion;
        $this->$opcion();

    }
}

 ?>