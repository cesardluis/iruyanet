<?php
class clientes{
	var $id;
	var $mysql;
	var $opcion;
	var $vars;
	public function __construct($vars= array()){
		$this->mysql = new MysqlDB();
		$this->vars = $vars;
		if(isset($vars['opcion']))
			$this->opcion = $vars['opcion'];
		else
			$this->opcion = 'listaClientes';

	}
	private function listaClientes(){
		$where = "";
		if ($_SESSION['session']['localidad'] != "all") {
			$where ="WHERE clientes.localidad = ".$_SESSION['session']['localidad'];
		}
		$clientes = $this->mysql->query("SELECT clientes.*,
			(select count(*) from host where host.idCliente = clientes.id) as hosts,
			grupos.nombre as grupo
			FROM clientes LEFT JOIN  grupos ON (clientes.grupo=grupos.id) $where ORDER BY clientes.nombre");

        header("content-type: application/x-javascript");
        echo json_encode($clientes);
	}

	private function nuevoCliente(){
		$grupos = $this->mysql->query("SELECT * FROM grupos");
		$grupoTxt = '';
		foreach ($grupos as $g) {
			$grupoTxt .= '<option value="'.$g['id'].'">'.$g['nombre'].'</option>';
		}

		$localidades = $this->mysql->query("SELECT * FROM localidades");
		$locaclidadTxt = '';
		foreach ($localidades as $l) {
			$locaclidadTxt .= '<option value="'.$l['id'].'">'.$l['nombre'].'</option>';
		}
		$opciones= array(
			'metodo' => "nuevoClienteProcesar",
			'grupo' => $grupoTxt,
			'localidad' => $locaclidadTxt,
			'abono' => 200,
			'ingreso' => date("d-m-Y")

 		);
		new Template ("clientes", $opciones);
	}

	private function nuevoClienteProcesar(){
	//	print_r($this->vars);
		$datos = array(
			'nombre' => htmlentities($this->vars['nombre']),
			'email' => $this->vars['email'],
			'direccion' => htmlentities($this->vars['direccion']),
			'celu' => $this->vars['celu'],
			'localidad' => $this->vars['localidad'],
			'ingreso' => date("Y-m-d", strtotime($this->vars['ingreso'])),
			'grupo' => $this->vars['grupo'],
			'antena' => $this->vars['antena'],
			'abono' => $this->vars['abono']
		);
		$result = $this->mysql->insert('clientes',$datos);
		if ($result) {
			echo "El Cliente se Ingreso correctamente";
		}
	}
	private function editarCliente(){
		$cliente = $this->mysql->query("SELECT *, DATE_FORMAT(ingreso, '%d-%m-%Y') as ingreso FROM clientes WHERE id =". $this->vars['id']);
 		$grupos = $this->mysql->query("SELECT * FROM grupos");
		$grupoTxt = '';
		foreach ($grupos as $g) {
			if ($g['id'] == $cliente[0]['grupo']) {
				$grupoTxt .= '<option value="'.$g['id'].'" selected="selected">'.$g['nombre'].'</option>';
			} else
				$grupoTxt .= '<option value="'.$g['id'].'">'.$g['nombre'].'</option>';
		}

		$localidades = $this->mysql->query("SELECT * FROM localidades");
		$locaclidadTxt = '';
		foreach ($localidades as $g) {
			if ($g['id'] == $cliente[0]['localidad']) {
				$locaclidadTxt .= '<option value="'.$g['id'].'" selected="selected">'.$g['nombre'].'</option>';
			} else
				$locaclidadTxt .= '<option value="'.$g['id'].'">'.$g['nombre'].'</option>';
		}

		$opciones= array(
 			'metodo' => "editarClienteProcesar&id=".$this->vars['id'],
 			'nombre' => $cliente[0]['nombre'],
 			'celu' => $cliente[0]['celu'],
			'email' => $cliente[0]['email'],
			'direccion' => $cliente[0]['direccion'],
			'ingreso' => $cliente[0]['ingreso'],
			'grupo' => $grupoTxt,
			'localidad' => $cliente[0]['localidad'],
			'antena' => $cliente[0]['antena'],
			'abono' => $cliente[0]['abono'],
			'localidad' => $locaclidadTxt

 		);
		new Template ("clientes", $opciones);

	}
	private function editarClienteProcesar(){
		$datos = array(
			'nombre' => htmlentities($this->vars['nombre']),
			'email' => $this->vars['email'],
			'direccion' => htmlentities($this->vars['direccion']),
			'celu' => $this->vars['celu'],
			'localidad' => $this->vars['localidad'],
			'ingreso' => date("Y-m-d", strtotime($this->vars['ingreso'])),
			'grupo' => $this->vars['grupo'],
			'antena' => $this->vars['antena'],
			'abono' => $this->vars['abono']
		);
		$this->mysql->where('id',$this->vars['id']);
		$result = $this->mysql->update('clientes',$datos);
		if ($result) {
			echo "El Cliente se ha modificado correctamente";
		}else
			echo "error. ";
	}
	private function nuevoHost(){
		$opciones= array(
			'metodo' => "nuevoHostProcesar&idCliente=".$this->vars['idCliente'],
			'estado' => '<option value="activo">Activo</option><option value="inactivo">Inactivo</option>',
			'mac' => $this->getMac()
 		);
		new Template ("host", $opciones);
	}
	private function nuevoHostProcesar(){
		$datos = array(
			'idCliente' => $this->vars['idCliente'],
			'nombre' => htmlentities($this->vars['nombre']),
			'mac' =>strtoupper( str_replace("-", ":", $this->vars['mac']) ),
			'estado' => $this->vars['estado']
		);
		if(preg_match('/([a-fA-F0-9]{2}[:|\-]?){6}/', $datos['mac']) == 1){
			$result = $this->mysql->insert('host',$datos);
			if ($result)
				echo "El Host se Ingreso correctamente";
		}else{
			$opciones= array(
				'metodo' => "nuevoHostProcesar&idCliente=".$this->vars['idCliente'],
				'estado' => '<option value="activo">Activo</option><option value="inactivo">Inactivo</option>',
				'error' => 'MAC Address invalido!!'
	 		);
			new Template ("host", array_merge($datos, $opciones));
		}
	}
	private function editarHost(){
		$host = $this->mysql->query("SELECT * FROM host WHERE id =". $this->vars['id']);
		$estado = array('activo' => 'Activo', 'inactivo' => 'Inactivo');
		$estadoTxt = '';
		foreach ($estado as $key => $value) {
			if ($key == $host[0]['estado']) {
				$estadoTxt .= '<option value="'.$key.'" selected="selected">'.$value.'</option>';
			} else
				$estadoTxt .= '<option value="'.$key.'">'.$value.'</option>';
		}

		$ip = $this->getIp($host[0]['mac']);
		if ($ip != '') $host[0]['ip'] = $ip;

		$opciones= array(
 			'metodo' => "editarHostProcesar&id=".$this->vars['id'],
			'estado' => $estadoTxt
 		);
		new Template ("host", array_merge($host[0],$opciones));

	}
	private function editarHostProcesar(){
		$datos = array(
			'nombre' => htmlentities($this->vars['nombre']),
			'mac' => strtoupper( str_replace("-", ":", $this->vars['mac']) ),
			'estado' => $this->vars['estado']
		);

		if(preg_match('/([a-fA-F0-9]{2}[:|\-]?){6}/', $datos['mac']) == 1){
			$this->mysql->where('id',$this->vars['id']);
			$result = $this->mysql->update('host',$datos);
			if ($result)
				echo "El Host se guardo correctamente";
		}else{

			$estado = array('activo' => 'Activo', 'inactivo' => 'Inactivo');;
			$estadoTxt = '';
			foreach ($estado as $key => $value) {
				if ($key == $datos['estado']) {
					$estadoTxt .= '<option value="'.$key.'" selected="selected">'.$value.'</option>';
				} else
					$estadoTxt .= '<option value="'.$key.'">'.$value.'</option>';
			}

			$opciones= array(
				'metodo' => "nuevoHostProcesar&idCliente=".$this->vars['idCliente'],
				'estado' => '<option value="activo">Activo</option><option value="inactivo">Inactivo</option>',
				'error' => 'MAC Address invalido!!'
	 		);
			new Template ("host", array_merge($datos, $opciones));
		}
	}
	private function verHosts(){
		$hosts = $this->mysql->query("SELECT * FROM host WHERE idCliente =". $this->vars['id']);

		$lista = '';
		foreach ($hosts as $key => $host) {
			$ip = $this->getIp($host['mac']);
			if ($ip == '') $ip = $host['ip'];
			$lista .= '<tr><td>'.$host['nombre'].'</td><td>'.$host['mac'].'</td><td>'.$ip.'</td>'.'</td><td>'.$host['estado'].'</td>';
			$lista .= '<td class="text-right"><a href="accion.php?name=clientes&opcion=eliminarHost&id='.$host['id'].'" class="button tiny alert round">eliminar</a>';
			$lista .= '<a href="accion.php?name=clientes&opcion=editarHost&id='.$host['id'].'" class="button tiny secondary round">editar</a></td></tr>';
		}

		new Template ("verhost", array('lista' => $lista, 'idCliente' => $this->vars['id']));


	}
	private function eliminarHost(){
		echo "Esta seguro que quiere eliminar este Host?<br>";
		echo '<a href="accion.php?name=clientes&opcion=eliminarHostProcesar&id='.$this->vars['id'].'">Si</a> | <a href="javascript:window.history.back();">No</a>' ;

	}
	private function eliminarHostProcesar(){
		$this->mysql->where('id',$this->vars['id']);
		$result = $this->mysql->delete('host');
		if ($result)
		echo "Host Eliminado!";
	}

	private function eliminarCliente(){
		echo "Esta seguro que quiere eliminar este Cliente?<br> Se eliminaran tambien los host asociados a este cliente.<br><br>";
		echo '<a href="accion.php?name=clientes&opcion=eliminarClienteProcesar&id='.$this->vars['id'].'">Si</a> | <a href="javascript:window.parent.$(\'#winModal\').foundation(\'reveal\', \'close\');">No</a>' ;

	}
	private function eliminarClienteProcesar(){
		$this->mysql->where('id',$this->vars['id']);
		$result = $this->mysql->delete('clientes');

		$this->mysql->where('idCliente',$this->vars['id']);
		$result = $this->mysql->delete('host');
		if ($result)
		echo "Cliente Eliminado!";
		echo "<script type='text/javascript'>
			window.parent.$('tr#id_".$this->vars['id']."').hide();
			window.parent.$('#winModal').foundation('reveal', 'close');
		</script>";
	}
	private function getIp($mac){
		$ip = "";
		$respuesta = exec("sudo ip ne | grep ".strtolower($mac));
		$rep = explode(" ", $respuesta);

		foreach ($rep as $val) {
			if (preg_match('/^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/',$val))
				$ip = $val;
		}
		return $ip;
	}
	private function getMac(){
	    if( isset( $_SERVER['HTTP_X_FORWARDED_FOR'] )) $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    else if( isset( $_SERVER ['HTTP_VIA'] ))  $ip = $_SERVER['HTTP_VIA'];
	    else if( isset( $_SERVER ['REMOTE_ADDR'] ))  $ip = $_SERVER['REMOTE_ADDR'];
	    else $ip = null ;

	    $mac = '';
		$respuesta = exec("sudo arp $ip");
		$rep = str_replace("-", ":", $respuesta);
		if (preg_match("/[0-9a-f][0-9a-f][:][0-9a-f][0-9a-f][:][0-9a-f][0-9a-f][:][0-9a-f][0-9a-f][:][0-9a-f][0-9a-f][:][0-9a-f][0-9a-f]/",$rep, $coincidencias))
			$mac = $coincidencias[0];
		return strtoupper($mac);
	}
	public function mostrar(){
        $opcion = $this->opcion;
        $this->$opcion();

    }
}

 ?>