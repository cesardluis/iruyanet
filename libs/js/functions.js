function active(linkActive){
    $("nav a").each(function(){
        if(this == linkActive)
            $(this).addClass("active");
        else
            $(this).removeClass("active");
    });
}
/* ************************************************************************************/

function html_clientes(data){
    if(data.length>0){
        $.each(data, function(i,v){
            var fecha = v.ingreso.split("-");
            $("#contenedor tbody").append("<tr id='id_"+ v.id +"'>"+
                "<td style='width: 15px'></td>"+
                "<td style='width: 35px'><a id='edit"+v.id+"'class='nwin editar' title='Editar Cliente - "+v.nombre+"' rev='600' href='accion.php?name=clientes&opcion=editarCliente&id="+ v.id+"'></a> "+
                "<a id='delet"+v.id+"'class='nwin eliminar' title='Eliminar Cliente' rev='150' href='accion.php?name=clientes&opcion=eliminarCliente&id="+v.id+"'></a></td>"+
                "<td>" + v.nombre + "</td>"+
                "<td>" + v.ingreso /*fecha[2] +'-'+ fecha[1] +'-'+ fecha[0]*/ + "</td>"+
                "<td>" + v.direccion + "</td>"+
                "<td>" + v.grupo + "</td>"+
                "<td>" + v.hosts + " "+
                "<a id='host"+v.id+"'class='nwin host' title='"+v.nombre+"' rev='400' href='accion.php?name=clientes&opcion=verHosts&id="+ v.id+"'></a></td>"+
                "</tr>");
        });
    }else $("#contenedor tbody").append("<tr class='tr5'><td colspan='6'><b>Aun no hay Clientes cargados.</b></td></tr>");
    $(".cargando").hide("fast").remove();
    propiedadesTabla(data.length, "#clientesT");
}
function html_hosts(data){
    if(data.length>0){
        $.each(data, function(i,v){
            var bullet = "bullet_on",
            ip = v.ip;
            if (v.ip == undefined){
                var bullet = "bullet_no_conectado";
                ip = 'No Conectado!'
            }

            $("#contenedor tbody").append("<tr>"+
                    "<td style='width: 15px'></td>"+
                    "<td>" + v.cliente + "</td>"+
                    "<td>" + v.nombre + "</td>"+
                    "<td>" + v.mac + "</td>"+
                    "<td><span class='"+bullet+"'></span>" + ip + "</td>"+
                    "<td>" + v.antena + "</td>"+
                    "</tr>");
        });
    }else $("#contenedor tbody").append("<tr class='tr5'><td colspan='6'><b>No hay Clientes Conectados.</b></td></tr>");
    $(".cargando").hide("fast").remove();
    propiedadesTabla(data.length, "#hostsT");
}

function propiedadesTabla(num, tabla){
    if(num>0){
        $(tabla).dataTable({
            bAutoWidth: false,
            iDisplayLength: 30,
            oLanguage: {
                sLengthMenu: "",
                sZeroRecords: "",
                sAutoWidth: false,
                bLengthChange: false,
                sInfo: "Mostrando _START_ a _END_ de _TOTAL_",
                sInfoEmpty: "0 Servicios Encontrados.",
                sInfoFiltered: "(filtered from _MAX_ total records)",
                sSearch: "",
                oPaginate: {sNext: "", sFirst: "Primero", sLast: "Ultimo",sPrevious: ""}
            },
            "fnDrawCallback": function ( oSettings ) {
                if ( oSettings.bSorted || oSettings.bFiltered )
                    for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
                        $('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
            }
        });
    }
    //$("a").tooltip({ track: true, position: {my: "center top+18",at: "center top"} });
}

$(document).ready(function(){
    $("nav a").click(function(){
       active(this);
    });
    $("a.json_request").click(function(e){
        e.preventDefault();
        $("<span class=\"cargando\"> Cargando...</span>").appendTo("#content").show("fast");
        $('nav.sub ul').hide();
        funcion = "html_"+$(this).attr("href");
        $.ajax({
            type: "GET", url: "templates/html/"+funcion+".html", dataType: "html",
            success: function(template){
                var template = $(template);
                $("nav.sub ul").html($("ul.menu", template).html());
                $("#contenedor").html($("div.cuerpo", template).html());
                $("nav.sub ul").show("fast");
                $(".cargando").hide("fast").remove();
            }
        });
        $.getJSON("accion.php?name=" + $(this).attr('href'), {}, eval(funcion));
    });
    $("a.ajax_request").click(function(e){
        e.preventDefault();
        $('nav.sub ul').hide();
        $.ajax({
            type: "GET",
            url: "accion.php?name="+$(this).attr('href'),
            dataType: "html",
            success: function(html){
                $("#contenedor").html(html);
                if ($("#importar_csv").length) ajaxFileUpload();
                if ($("#mapa").length) mapa();
                $(".chzn-select").chosen({allow_single_deselect:true});
            }
        });
    });
    $("#preload").hide().remove();

    $("nav ul li:nth-child(1) a").trigger('click');

    $(document).on('click', '#contenedor tr', function(event) {
        if(!$(this).hasClass("selected"))
            $(this).addClass("selected");
        else
            $(this).removeClass("selected");
    });

    $(document).on('click', '#contenedor tr', function(event) {
        if(!$(this).hasClass("hover"))
            $(this).addClass("hover");
        else
            $(this).removeClass("hover");

    });
    winModal();
});