function winModal(){

    $(document).on('click', 'a.nwin', function(event) {
        event.preventDefault();
        var size = $(this).attr("rev"),
            item = {
            id: "win"+$(this).attr("id"),
            title: $(this).attr("title"),
            url: $(this).attr("href"),
            size: (size >= 400) ? "medium": "tiny"
        };
        var iframe = "<iframe id='iframeModal' align='middle' width='100%' height='100%' frameborder='0' marginwidth='0' marginheight='0'"
                    +"src='"+ item.url +"'></iframe>";
        $("#winModal .content").css('min-height', size+'px');
        $("#winModal .content").html(iframe);
        $("#winModal h4").text(item.title);
        $("#winModal").removeClass('medium tiny').addClass(item.size);
        $("#winModal").foundation('reveal', 'open');
        $("#iframeModal").height($("#winModal").height());
    });
    $('a.close-reveal-modal').on('click', function(event) {
        $("#winModal").foundation('reveal', 'close');
    });;
}
